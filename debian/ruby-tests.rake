require 'gem2deb/rake/testtask'

#ENV['AR'] = '1'

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['test']
  t.test_files = FileList['test/app_*.rb']
end
